## Getting started

1. Clone the project
`git clone git@gitlab.com:ericapisani/guelph-conf-2018.git`

2. Take a look at the issues board
https://gitlab.com/ericapisani/guelph-conf-2018/boards

3. Pick an issue you'd like to work on from the 'To Do' list by assigning yourself to that issue and moving it to the 'Doing' list. Fork a branch off of the master branch to do you work.

4. Once you're ready for a quick review, open a merge request again the `master` branch, and move your issue into the 'Waiting for Review' list.  Once it passes tests and gets a +1 from somebody, it'll get merged by someone with write access on the `master` branch.

5. Repeat steps 2-4 as needed.

If you have any questions about anything, ping me on Slack.


## Running the project locally
1. Run `npm install` to install all the packages.

2. Run `npm start` to start running the development server. If you make any changes to the files, the server will automatically restart as it's run with `webpack`. There's more details in the README as this project was created with `create-react-app`.

## Tooling that we're using:
* [reactstrap](https://reactstrap.github.io/) (React Bootstrap 4 components)
* [Font Awesome](https://fontawesome.com/)

## Colours for the Site:
* Main background - #F2EFEA
* Nav bar background - #FF6B6B

