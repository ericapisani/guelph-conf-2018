## Overview
Give us a brief overview of what the problem is

## Steps to reproduce
List the steps you took in point form to cause this bug to occur.

## Expected behaviour
Explain what you expect to see when you take the steps listed above


## Actual behaviour
Explain what you're seeing. Include screenshots/GIFs if possible of what the issue looks like


## Any suspicions on where you think this might be coming from
If you believe you know the root cause of the issue or have some ideas of where this might be coming from, list them here so if someone other than you is taking a look at it, they have a starting point.
