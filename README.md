# GryphTechCon Website

* Created with `create-react-app`

## Setup
* Clone repo
* Run `nvm use` to ensure that you're on the correct version of node
* Run `npm install`
* Run `npm run start`
