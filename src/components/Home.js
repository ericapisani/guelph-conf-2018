import React, { Component } from 'react';

import OptimizedNetworkCardImage from '../images/webp/network_card_image.webp';
import NetworkCardImage from '../images/network_card_image.jpg';

import OptimizedHomeImage from '../images/webp/home.webp';
import HomeImage from '../images/home.jpg';

import OptimizedRecruitCardImage from '../images/webp/recruit_card_image.webp';
import RecruitCardImage from '../images/recruit_card_image.jpg';

import OptimizedVolunteerCardImage from '../images/webp/volunteer_card_image.webp';
import VolunteerCardImage from '../images/volunteer_card_image.jpg';

import { createBannerImage } from './BannerImage';

import {
  Container,
  Row,
  Col,
  Card,
  CardDeck,
  CardText,
  CardBody,
  CardTitle,
  Button
} from 'reactstrap';

import '../styles/css/Home.css';

export default class Home extends Component {
  render() {
    return (
      <div>
        <Container className="home-content">
          {createBannerImage(
            {
              image: HomeImage,
              optimizedImage: OptimizedHomeImage,
              altText: "Computer on desk",
              headerText: "GryphTechCon 2019",
              photographerDetails: {
                name: "Émile Perron",
                link: "https://unsplash.com/@emilep"
              }
            }
          )}
          <Row>
            <Col className="home__summary">
              Highlighting the latest trends in the industry to future software professionals, and providing a friendly and welcoming space for those looking to practice their talks.
            </Col>
          </Row>
          <Row className="home__cards">
            <CardDeck>
              <Card>
                <picture>
                  <source className="card-img-top" width="100%" srcSet={OptimizedNetworkCardImage} type="image/webp" />
                  <img className="card-img-top" width="100%" src={NetworkCardImage} alt="Talks by Alumni and Students card" />
                </picture>
                <span className="card__image-credit">Photo by <a className="image__credit" href="https://unsplash.com/@cikstefan">Štefan Štefančík</a></span>
                <CardBody>
                  <CardTitle>Talks by Alumni and Students</CardTitle>
                  <CardText>Hear about the latest trends in the industry from alumni and your peers, research by graduate students, and more! Want to try your hand at giving a talk? This is the conference for you!</CardText>
                  <a href="/speakers"><Button color="info">See list of speakers</Button></a>
                </CardBody>
              </Card>
              <Card>
                <picture>
                  <source className="card-img-top" width="100%" srcSet={OptimizedRecruitCardImage} type="image/webp" />
                  <img className="card-img-top" width="100%" src={RecruitCardImage} alt="Network with alumni and students" />
                </picture>
                <span className="card__image-credit">Photo by <a className="image__credit" href="https://unsplash.com/@priscilladupreez">Priscilla Du Preez</a></span>
                <CardBody className="d-flex align-items-start flex-column">
                  <CardTitle>Network with Alumni and Students</CardTitle>
                  <CardText>Meet with prospective co-op and summer students as well as recent and not-so-recent grads.</CardText>
                  <a href="mailto:info@gryphtechcon.com" className="btn btn-info mt-auto" role="button" aria-pressed="true">Get additional info</a>
                </CardBody>
              </Card>
              <Card>
                <picture>
                  <source className="card-img-top" width="100%" srcSet={OptimizedVolunteerCardImage} type="image/webp" />
                  <img className="card-img-top" width="100%" src={VolunteerCardImage} alt="Contact us" />
                </picture>
                <span className="card__image-credit">Photo by <a className="image__credit" href="https://unsplash.com/@wildlittlethingsphoto">Helena Lopes</a></span>
                <CardBody className="d-flex align-items-start flex-column">
                  <CardTitle>Interested in Speaking, Sponsoring, or Volunteering?</CardTitle>
                  <CardText>If you'd like additional information, feel free to contact us!</CardText>
                  <a href="mailto:info@gryphtechcon.com" className="btn btn-info mt-auto" role="button" aria-pressed="true">Contact us</a>
                </CardBody>
              </Card>
            </CardDeck>
          </Row>
        </Container>
      </div>
    );
  }
}
