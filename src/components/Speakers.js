import React, { Component } from 'react';

import OptimizedSpeakersImage from '../images/webp/speakers.webp';
import SpeakersImage from '../images/speakers.jpg';

import { createBannerImage } from './BannerImage';

import {
  Container,
  Row,
  Col,
  Button
} from 'reactstrap';

import '../styles/css/Speakers.css';
export default class Speakers extends Component {
  render() {
    return (
      <div>
        <Container className="speakers-content">
        {createBannerImage(
            {
              image: SpeakersImage,
              optimizedImage: OptimizedSpeakersImage,
              altText: "Microphone",
              headerText: "Speakers",
              photographerDetails: {
                name: "Elliot Sloman",
                link: "https://unsplash.com/@esloman"
              }
            }
          )}
          <Row>
            <Col className="speakers__summary">
              <p>
                GryphTechCon offers the opportunity to get experience in public speaking.
                You can use your experience with us to help you land further speaking engagements.
              </p>

              <p>
                If you've never done public speaking before but want to get better at it, we can help with strategies or ideas.
              </p>

              <p>
                By giving a talk, you will be helping bring a SoCS alumni presence onto campus and help fellow students navigate their degree and transition to industry.
              </p>
            </Col>
          </Row>
          <Row>
            <Col align="center" className="speakers__contact-btn">
              <a href="mailto:speakers@gryphtechcon.com">
                <Button size="lg" color="info">Contact us about speaking opportunities</Button>
              </a>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
