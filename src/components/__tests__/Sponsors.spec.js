import React from 'react';
import TestRenderer from 'react-test-renderer';
import Sponsors from '../Sponsors';

describe('Sponsors component', () => {
  it('renders', () => {
    const testRenderer = TestRenderer.create(<Sponsors />);
    expect(testRenderer.toJSON()).toMatchSnapshot();
  });
});
