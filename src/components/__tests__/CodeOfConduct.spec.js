import React from 'react';
import TestRenderer from 'react-test-renderer';
import CodeOfConduct from '../CodeOfConduct';

describe('CodeOfConduct component', () => {
  it('renders', () => {
    const testRenderer = TestRenderer.create(<CodeOfConduct />);
    expect(testRenderer.toJSON()).toMatchSnapshot();
  });
});
