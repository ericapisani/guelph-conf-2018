import React from 'react';
import TestRenderer from 'react-test-renderer';
import Schedule from '../Schedule';

describe('Schedule component', () => {
  it('renders', () => {
    const testRenderer = TestRenderer.create(<Schedule />);
    expect(testRenderer.toJSON()).toMatchSnapshot();
  });
});
