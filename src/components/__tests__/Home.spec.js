import React from 'react';
import TestRenderer from 'react-test-renderer';
import Home from '../Home';

describe('Home component', () => {
  it('renders', () => {
    const testRenderer = TestRenderer.create(<Home />);
    expect(testRenderer.toJSON()).toMatchSnapshot();
  });
});
