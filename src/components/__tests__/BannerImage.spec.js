import { createBannerImage } from '../BannerImage';

describe('BannerImage', () => {
  it('renders', () => {
    const testData = {
      image: 'some-image',
      altText: 'some-alt-text',
      headerText: 'some-header-text',
      photographerDetails: 'a photographer',
    };
    expect(createBannerImage(testData)).toMatchSnapshot();
  });
});
