import React from 'react';
import TestRenderer from 'react-test-renderer';
import Team from '../Team';

describe('Team component', () => {
  it('renders', () => {
    const testRenderer = TestRenderer.create(<Team />);
    expect(testRenderer.toJSON()).toMatchSnapshot();
  });
});
