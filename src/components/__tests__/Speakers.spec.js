import React from 'react';
import TestRenderer from 'react-test-renderer';
import Speakers from '../Speakers';

describe('Speakers component', () => {
  it('renders', () => {
    const testRenderer = TestRenderer.create(<Speakers />);
    expect(testRenderer.toJSON()).toMatchSnapshot();
  });
});
