import React from 'react';
import TestRenderer from 'react-test-renderer';
import Venue from '../Venue';

describe('Venue component', () => {
  it('renders', () => {
    const testRenderer = TestRenderer.create(<Venue />);
    expect(testRenderer.toJSON()).toMatchSnapshot();
  });
});
