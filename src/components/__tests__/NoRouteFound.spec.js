import React from 'react';
import TestRenderer from 'react-test-renderer';
import NoRouteFound from '../NoRouteFound';

describe('NoRouteFound component', () => {
  it('renders', () => {
    const testRenderer = TestRenderer.create(<NoRouteFound />);
    expect(testRenderer.toJSON()).toMatchSnapshot();
  });
});
