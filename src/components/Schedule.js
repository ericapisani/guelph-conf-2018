import React, { Component } from 'react';

import {
  Container,
  Row,
  Col
} from 'reactstrap';

import '../styles/css/Schedule.css';

export default class Schedule extends Component {
  render() {
    return (
      <div>
        <Container>
          <Row>
            <Col className="header">
              <h2>Schedule</h2>
            </Col>
          </Row>
          <Row>
            <Col className="content">
              Coming soon!
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
