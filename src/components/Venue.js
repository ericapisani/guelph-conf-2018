import React, { Component } from 'react';
import {
  Container,
  Row,
  Col
} from 'reactstrap';

import { createBannerImage } from './BannerImage';

import OptimizedVenueImage from '../images/webp/university_of_guelph.webp';
import VenueImage from '../images/university_of_guelph.jpg';

import '../styles/css/Venue.css';

export default class Venue extends Component {
  render() {
    return (
      <div>
        <Container className="venue-content">
        {createBannerImage(
            {
              image: VenueImage,
              optimizedImage: OptimizedVenueImage,
              altText: "Johnson Hall",
              headerText: "Venue",
              photographerDetails: {
                name: "source",
                link: "https://commons.wikimedia.org/wiki/File:Johnston_Hall_University_of_Guelph.JPG"
              }
            }
          )}
          <Row>
            <Col className="venue__summary">
              <p>
                GryphTechCon will be held at the main campus at the University of Guelph. Further details TBD.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
