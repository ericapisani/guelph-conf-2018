import React, { Component } from 'react';
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  CardDeck,
  CardText,
  CardBody,
  CardTitle,
} from 'reactstrap';

import { createBannerImage } from './BannerImage';

import OptimizedTeamImage from '../images/webp/team.webp';
import TeamImage from '../images/team.jpg';

import OptimizedEricaTeamImage from '../images/webp/erica_team_photo.webp';
import EricaTeamImage from '../images/erica_team_photo.jpg';

import OptimizedAndrewTeamImage from '../images/webp/andrew_team_photo.webp';
import AndrewTeamImage from '../images/andrew_team_photo.jpg';

import OptimizedAmarisTeamImage from '../images/webp/amaris_team_photo.webp';
import AmarisTeamImage from '../images/amaris_team_photo.jpg';

import OptimizedNisargTeamImage from '../images/webp/nisarg_team_photo.webp';
import NisargTeamImage from '../images/nisarg_team_photo.jpg';

import '../styles/css/Team.css';

export default class Team extends Component {
  render() {
    return (
      <div>
        <Container className="team-content">
        {createBannerImage(
            {
              image: TeamImage,
              optimizedImage: OptimizedTeamImage,
              altText: "Team",
              headerText: "Team",
              photographerDetails: {
                name: "Raw Pixel",
                link: "https://unsplash.com/@rawpixel"
              }
            }
          )}
          <Row className="team__cards">
            <CardDeck>
              <Card>
                <picture>
                  <source className="card-img" srcSet={OptimizedEricaTeamImage} type="image/webp" />
                  <img className="card-img" src={EricaTeamImage} alt="Erica Pisani" />
                </picture>
                <CardBody>
                  <CardTitle>Erica Pisani</CardTitle>
                  <CardText>More info coming!</CardText>
                </CardBody>
              </Card>
              <Card>
                <picture>
                  <source className="card-img" srcSet={OptimizedAndrewTeamImage} type="image/webp" />
                  <img className="card-img" src={AndrewTeamImage} alt="Andrew Welton" />
                </picture>
                <CardBody>
                  <CardTitle>Andrew Welton</CardTitle>
                  <CardText>More info coming!</CardText>
                </CardBody>
              </Card>
              <Card>
                <picture>
                  <source className="card-img" srcSet={OptimizedAmarisTeamImage} type="image/webp" />
                  <img className="card-img" src={AmarisTeamImage} alt="Amaris Gerson" />
                </picture>
                <CardBody>
                  <CardTitle>Amaris Gerson</CardTitle>
                  <CardText>More info coming!</CardText>
                </CardBody>
              </Card>
              <Card>
                <picture>
                  <source className="card-img" srcSet={OptimizedNisargTeamImage} type="image/webp" />
                  <img className="card-img" src={NisargTeamImage} alt="Nisarg Dalvi" />
                </picture>
                <CardBody>
                  <CardTitle>Nisarg Dalvi</CardTitle>
                  <CardText>More info coming!</CardText>
                </CardBody>
              </Card>
            </CardDeck>
          </Row>
          <Row>
            <Col align="center" className="team__contact-btn">
              <a href="mailto:info@gryphtechcon.com">
                <Button size="lg" color="info">Interested in volunteering?</Button>
              </a>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
