import React, { Component } from 'react';
import {
  Container,
  Row,
  Col
} from 'reactstrap';

import '../styles/css/CodeOfConduct.css';

export default class CodeOfConduct extends Component {
  render() {
    return (
      <div>
        <Container className="code-of-conduct-content">
          <Row>
            <Col className="header">
              <h2>Code of Conduct</h2>
            </Col>
          </Row>
          <Row>
            <Col className="content">
              <p>
                GryphTechCon is a community conference intended for networking and collaboration in the developer community.
              </p>
              <p>
                We value the participation of each member of the Guelph and surrounding communities and
                want all attendees to have an enjoyable and fulfilling experience.
                Accordingly, all attendees are expected to show respect and courtesy to other attendees throughout the
                conference and at all conference events, whether officially sponsored by GryphTechCon or not.
              </p>
              <p>
                To make clear what is expected, all delegates, speakers, exhibitors and volunteers at
                any GryphTechCon event are required to conform to the following Code of Conduct.
                Organizers will enforce this code throughout the event.
              </p>
            </Col>
          </Row>
          <Row>
            <Col className="content">
              <h3>The Short Version</h3>

              <p>
                GryphTechCon is dedicated to providing a harassment-free conference experience for everyone,
                regardless of gender, sexual orientation, disability, physical appearance, body size,
                race or religion. We do not tolerate harassment of conference participants in any form.
              </p>

              <p>
                All communication should be appropriate for a professional audience including people of
                many different backgrounds. Sexual language and imagery is not appropriate for any
                conference venue, including talks.
              </p>

              <p>
                Be kind and respectful to others. Do not insult or put down other attendees.
                Behave professionally. Remember that harassment and sexist, racist, or
                exclusionary jokes are not appropriate for GryphTechCon.
              </p>

              <p>
                Attendees violating these rules may be asked to leave the conference without a refund
                at the sole discretion of the conference organizers.
              </p>

              <p>
                Thank you for helping make this a welcoming, friendly event for all.
              </p>
            </Col>
          </Row>
          <Row>
            <Col className="content">
              <h3>The Longer Version</h3>

              <p>
                Harassment includes offensive verbal comments related to gender,
                sexual orientation, disability, physical appearance, body size,
                race, religion, sexual images in public spaces,
                deliberate intimidation, stalking, following, harassing photography or
                recording, sustained disruption of talks or other events, inappropriate physical contact,
                and unwelcome sexual attention.
              </p>

              <p>
                Participants asked to stop any harassing behavior are expected to comply immediately.
              </p>

              <p>
                Exhibitors in the venue, sponsor or vendor booths, or similar activities are
                also subject to the anti-harassment policy. In particular,
                exhibitors should not use sexualized images, activities, or other material.
                Booth staff (including volunteers) should not use sexualized
                clothing/uniforms/costumes, or otherwise create a sexualized environment.
              </p>

              <p>
                Be careful in the words that you choose. Remember that sexist, racist,
                and other exclusionary jokes can be offensive to those around you.
                Excessive swearing and offensive jokes are not appropriate for GryphTechCon.
              </p>

              <p>
                If a participant engages in behavior that violates this code of conduct, the
                conference organizers may take any action they deem appropriate,
                including warning the offender or expulsion from the conference with no refund.
              </p>
            </Col>
          </Row>
          <Row>
            <Col className="content">
              <h3>Contact Information</h3>

              <p>
                If you are being harassed, notice that someone else is being harassed,
                or have any other concerns, please contact a member of conference staff.
              </p>

              <p>
                If the matter is especially urgent, please call/contact any of these individuals:

                <strong>To be posted closer to the conference date</strong>
              </p>

              <p>
                Conference staff will be happy to help participants contact venue security or
                local law enforcement, provide escorts, or otherwise assist those
                experiencing harassment to feel safe for the duration of the conference.
                We value your attendance.
              </p>
            </Col>
          </Row>
          <Row>
            <Col className="content">
              <h3>Attribution</h3>

              <p>
                This policy is based on the policy of <a href="https://2017.pycon.ca/code-of-conduct/">PyCon Canada </a>
                which in turn was based on the example policy
                from the <a href="http://geekfeminism.wikia.com/wiki/Conference_anti-harassment">Geek Feminism wiki</a>,
                created by the Ada Initiative and other volunteers. It is used under the terms of the
                <a href="https://creativecommons.org/publicdomain/zero/1.0/"> Creative Commons Zero license.</a>
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
