import React, { Component } from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faQuestion from '@fortawesome/fontawesome-free-solid/faQuestion'

import {
  Container,
  Row,
  Col,
} from 'reactstrap';

import '../styles/css/NoRouteFound.css';
export default class NoRouteFound extends Component {
  render() {
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <FontAwesomeIcon className="fa-icon" icon={faQuestion} size="5x" />
            </Col>
          </Row>
          <Row>
            <Col className="container--header">
              <h2>404 - Page Not Found</h2>
            </Col>
          </Row>
          <Row>
            <Col className="container--body">
              Hi there! A page doesn't exist here. Why not try clicking on one of the items in the nav bar above?
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
