import React from 'react';

import {
  Row,
  Col,
} from 'reactstrap';


export function createBannerImage({ image, optimizedImage, altText, headerText, photographerDetails }) {
  return (
    <Row className="image-row">
      <Col className="image">
        <picture>
          <source className="image__photo" srcSet={optimizedImage} type="image/webp" />
          <img className="image__photo" src={image} alt={altText} />
        </picture>
        <span><h2>{headerText}</h2></span>
        Photo by <a className="image__credit" href={photographerDetails.link}>{photographerDetails.name}</a>
      </Col>
    </Row>
  )
}
