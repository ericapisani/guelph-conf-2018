import React, { Component } from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faTerminal from '@fortawesome/fontawesome-free-solid/faTerminal'
import { Link } from 'react-router-dom'

import '../styles/css/NavigationBar.css';

import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

export default class NavigationBar extends Component {
  render() {
    return (
      <div className="navigationBar">
        <Navbar color="light">
          <NavbarBrand aria-label="Home page icon">
            <Link to="/">
              <FontAwesomeIcon icon={faTerminal} size="3x" />
            </Link>
          </NavbarBrand>
          <Nav>
            <NavItem>
              <NavLink><Link to="/code-of-conduct">Code of Conduct</Link></NavLink>
            </NavItem>
            <NavItem>
              <NavLink><Link to="/schedule">Schedule</Link></NavLink>
            </NavItem>
            <NavItem>
              <NavLink><Link to="speakers">Speakers</Link></NavLink>
            </NavItem>
            <NavItem>
              <NavLink><Link to="/sponsors">Sponsors</Link></NavLink>
            </NavItem>
            <NavItem>
              <NavLink><Link to="/venue">Venue</Link></NavLink>
            </NavItem>
            <NavItem>
              <NavLink><Link to="/team">Team</Link></NavLink>
            </NavItem>
          </Nav>
        </Navbar>
      </div>
    );
  }
}
