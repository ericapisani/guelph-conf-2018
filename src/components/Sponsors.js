import React, { Component } from 'react';
import {
  Container,
  Row,
  Col,
  Button
} from 'reactstrap';
import { createBannerImage } from './BannerImage';

import OptimizedSponsorsImage from '../images/webp/sponsors.webp';
import SponsorsImage from '../images/sponsors.jpg';

import '../styles/css/Sponsors.css';

export default class Sponsors extends Component {
  render() {
    return (
      <div>
        <Container className="sponsors-content">
        {createBannerImage(
            {
              image: SponsorsImage,
              optimizedImage: OptimizedSponsorsImage,
              altText: "People shaking hands",
              headerText: "Sponsors",
              photographerDetails: {
                name: "rawpixel",
                link: "https://unsplash.com/@rawpixel"
              }
            }
          )}
          <Row>
            <Col className="sponsors__summary">
              Interested in networking with students and alumni? We'll have booths available for sponsors of the event.
            </Col>
          </Row>
          <Row>
            <Col align="center" className="sponsors__contact-btn">
              <a href="mailto:info@gryphtechcon.com">
                <Button size="lg" color="info">Contact us about sponsoring</Button>
              </a>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
