import React from 'react';
import ReactDOM from 'react-dom';
import Home from './components/Home';
import Team from './components/Team';
import CodeOfConduct from './components/CodeOfConduct';
import Schedule from './components/Schedule';
import Speakers from './components/Speakers';
import Sponsors from './components/Sponsors';
import Venue from './components/Venue';
import NoRouteFound from './components/NoRouteFound';
import NavigationBar from './components/NavigationBar';

import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom'

import './styles/css/index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free-webfonts';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(
  <Router>
    <div>
      <NavigationBar />
      <Switch>
        <Route exact path='/' component={Home}></Route>
        <Route path='/team' component={Team}></Route>
        <Route path='/code-of-conduct' component={CodeOfConduct}></Route>
        <Route path='/schedule' component={Schedule}></Route>
        <Route path='/speakers' component={Speakers}></Route>
        <Route path='/sponsors' component={Sponsors}></Route>
        <Route path='/venue' component={Venue}></Route>
        <Route component={NoRouteFound}></Route>
      </Switch>
    </div>
  </Router>
  , document.getElementById('root'));
registerServiceWorker();
